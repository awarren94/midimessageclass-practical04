//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "LinkedList.h"

template <typename Type>
Type max (Type a, Type b);

template <typename Type>
Type min(Type a, Type b);

template <typename Type>
Type add(Type a, Type b);

template <typename Type>
void print(Type array[], int size);

template <typename Type>
Type getAverage (Type array[], int size);

int main (int argc, const char* argv[])
{
    float a, b;
    float array[5] = {0.f, 1.f, 2.f, 3.f, 4.f};
    
    std::cout << "insert two numbers:" << std::endl;
    std::cin >> a;
    std::cin >> b;
    std::cout << "max = " << max (a, b) << std::endl;
    std::cout << "min = " << min (a, b) << "\n";
    std::cout << "sum = " << add (a, b) << "\n\n";
    
    print (array, 5);
    
    std::cout << "average = " << getAverage(array, 5);
    return 0;
}

template <typename Type>
Type max(Type a,Type b)
{
    if(a < b)
        return b;
    else
        return a;
}

template <typename Type>
Type min(Type a, Type b)
{
    if(a < b)
        return a;
    else
        return b;
}

template <typename Type>
Type add(Type a, Type b)
{
    return a + b;
}

template <typename Type>
void print(Type array[], int size)
{
    std::cout << "string of length " << size << "\n\n";
    for(int i = 0; i < size; i ++)
        std::cout << array[i] << std::endl;
    
}

template <typename Type>
Type getAverage(Type array[], int size)
{
    Type average;
    Type sum = 0;
    
    for(int i = 0; i < size; i++){
        sum = sum + array[i];
            }
    average = 1.0 * sum / size;
    
    return average;
}

