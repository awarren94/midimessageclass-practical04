//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "LinkedList.h"
#include "Number.h"
#include "Array.h"



int main (int argc, const char* argv[])
{
    Array<float> numf;
    Array<int> numi;
    Array <std::string> numstr;
    
    numf.add(0.5f);
    numi.add (2);
    //numstr.add ("yes");
    
    numf.add(2.5f);
    numi.add (5);
    //numstr.add ("no");
 
    
    
    std::cout << numf.get(1) << "\n";
    std::cout << numi.get(1) << "\n";
    //std::cout << numstr.get(0) << "\n";

    numf.testArray();
    
    
    return 0;
}



