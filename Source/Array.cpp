//
//  Array.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 07/10/2014.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#include "Array.h"

Array::Array()
{
    arraySize = 0;
    data = nullptr;
}

Array::~Array()
{
    if (data != nullptr)
    {
        delete [] data;
    }
}

int Array::size() const
{
    return arraySize;
}

void Array::add (float value)
{
    float* tempPtr = new float[size() + 1];
    
    for (int i = 0; i < size(); i++)
        tempPtr[i] = data[i];
    
    tempPtr[size()] = value;
    
    if (data != nullptr)
        delete [] data;
    
    data = tempPtr;
    arraySize++;
}

float Array::get (int index) const
{
    if (index >= 0 && index < size())
        return data[index];
    else
        return 0.f;
}

void Array::remove (int index)
{
    if (index < 0 || index >= size())
        return;
    
    float* tempPtr = new float[size() - 1];
    
    for (int i = 0; i < size(); i++)
    {
        if (i < index)
            tempPtr[i] = data[i];
        else if (i > index)
            tempPtr[i - 1] = data[i];
    }
    
    delete [] data;
    data = tempPtr;
    arraySize--;
}

void Array::reverse()
{
    int halfArraySize = size() / 2; //odd numbers leave the middle element where it is
    for (int i = 0; i < halfArraySize; i++)
    {
        float temp = data[i];
        data[i] = data[size() - 1 - i];
        data[size() - 1 - i] = temp;
    }
}

#include <iostream>
bool Array::testArray()
{
    Array array;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
    
    if (array.size() != 0)
    {
        std::cout << "size is incorrect\n";
        return false;
    }
    
    for (int i = 0; i < testArraySize; i++)
    {
        array.add (testArray[i]);
        
        if (array.size() != i + 1)
        {
            std::cout << "size is incorrect\n";
            return false;
        }
        
        if (array.get (i) != testArray[i])
        {
            std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
        }
    }
    
    array.reverse();
    for (int i = 0; i < testArraySize; i++)
    {
        if (array.get(i) != testArray[testArraySize - 1 - i])
        {
            std::cout << "reverse did not work\n";
            return false;
        }
    }
    //put back forward
    array.reverse();
    
    //removing first
    array.remove (0);
    if (array.size() != testArraySize - 1)
    {
        std::cout << "with size after removing item\n";
        return false;
    }
    
    for (int i = 0; i < array.size(); i++)
    {
        if (array.get(i) != testArray[i+1])
        {
            std::cout << "problems removing items\n";
            return false;
        }
    }
    
    //removing last
    array.remove (array.size() - 1);
    if (array.size() != testArraySize - 2)
    {
        std::cout << "with size after removing item\n";
        return false;
    }
    
    for (int i = 0; i < array.size(); i++)
    {
        if (array.get(i) != testArray[i + 1])
        {
            std::cout << "problems removing items\n";
            return false;
        }
    }
    
    //remove second item
    array.remove (1);
    if (array.size() != testArraySize - 3)
    {
        std::cout << "with size after removing item\n";
        return false;
    }
    
    if (array.get (0) != testArray[1])
    {
        std::cout << "problems removing items\n";
        return false;
    }
    
    if (array.get (1) != testArray[3])
    {
        std::cout << "problems removing items\n";
        return false;
    }
    
    if (array.get (2) != testArray[4])
    {
        std::cout << "problems removing items\n";
        return false;
    }
    
    //    for (int i = 0; i < array.size(); i++)
    //    {
    //        std::cout << "value at index "<< i << " = " << array.get (i) << "\n";
    //    }
    
    std::cout << "all Array tests passed\n";
    return true;
}

