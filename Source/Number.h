//
//  Number.h
//  CommandLineTool
//
//  Created by Andrew Warren on 16/10/2015.
//  Copyright (c) 2015 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__Number__
#define __CommandLineTool__Number__

#include <stdio.h>
#include <iostream>

template <class Type>
class Number
{
public:
    Type get() const
    {
        std::cout << "yes value ";
        return value;
    }
    
    void set(Type newValue)
    {
        value = newValue;
    }

private:
    Type value;
    
   

};
#endif /* defined(__CommandLineTool__Number__) */
